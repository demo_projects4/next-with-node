import { GetServerSideProps, NextPage } from 'next';
import { User } from '@/types';
import React from 'react';
import { useRouter } from 'next/router';

interface UserPageProps {
    user: User;
}

const UserPage: NextPage<UserPageProps> = ({ user }) => {
    return (
        <div className="block m-auto w-1/2 p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 cursor-pointer">
            <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                {user.name}
            </h5>
        </div>
    );
};

export const getServerSideProps: GetServerSideProps<UserPageProps> = async (context) => {
    const res = await fetch(`http://localhost:3001/users/${context.query.id}`);
    const user = await res.json();
    return {
        props: {
            user: user,
        },
    };
};

export default UserPage;
