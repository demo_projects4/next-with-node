import Image from 'next/image'
import { Inter } from 'next/font/google'
import { GetServerSideProps, NextPage } from 'next'
import { User } from '@/types'
import Link from 'next/link';
import React from 'react';
import { useRouter } from 'next/router';

const inter = Inter({ subsets: ['latin'] })

interface UserInfo {
    name: string | null;
    email: string | null;
    avatar: string | null;
}

interface ModalComponentProps {
    setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
    handleSubmit: () => void;
    setUserInfo: React.Dispatch<React.SetStateAction<UserInfo>>;
    userInfo: UserInfo;
}

const Home: NextPage<{ users: User[] }> = ({ users }) => {

    // to handle modal
    const [showModal, setShowModal] = React.useState<boolean>(false);
    const [userList, setUserList] = React.useState<User[]>(users)

    // form data
    const [userInfo, setUserInfo] = React.useState<UserInfo>({
        name: null,
        email: null,
        avatar: null
    })

    // to handle next routing
    const router = useRouter();

    // to route user
    const handleClick = (id: number) => {
        router.push('/users/' + id);
    };

    // POST method implementation:
    async function postData(url: string = "", data: object = {}): Promise<any> {
        // Default options are marked with *
        const response = await fetch(url, {
            method: "POST", // *GET, POST, PUT, DELETE, etc.
            headers: {
                "Content-Type": "application/json",
                // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }

    // submit the user data
    const handleSubmit = () => {
        postData("http://localhost:3001/users", userInfo)
            .then((data) => {
                console.log(data); // JSON data parsed by `data.json()` call
                setUserList(data);

                // hide the modal
                setShowModal(false);
            })
            .catch((err) => console.log("err is ", err));
    };

    return (
        <>
            <main className={`flex min-h-screen flex-col items-center p-24 ${inter.className}`}>
                <button
                    className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-4"
                    onClick={() => setShowModal(!showModal)}
                >
                    Add User
                </button>

                {/* show user information */}
                <UsersTable users={userList} handleClick={handleClick} />
            </main>

            {/* modal */}
            {showModal && (
                <ModalComponent
                    setShowModal={setShowModal}
                    handleSubmit={handleSubmit}
                    setUserInfo={setUserInfo}
                    userInfo={userInfo}
                />
            )}
        </>
    );
};

export default Home;

// table component
interface UsersTableProps {
    users: User[];
    handleClick: (id: number) => void;
}

const UsersTable: React.FC<UsersTableProps> = ({ users, handleClick }) => {
    return users.map((user) => {
        return (
            <div
                className="block w-1/2 p-6 bg-white border border-gray-200 rounded-lg shadow hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700 cursor-pointer mb-2"
                key={user.id}
                onClick={() => handleClick(user.id)}
            >
                <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    {user.name}
                </h5>
            </div>
        );
    });
};

// modal
const ModalComponent: React.FC<ModalComponentProps> = ({ setShowModal, handleSubmit, setUserInfo, userInfo }) => {
    return (
        <>
            <div className="h-screen w-full bg-black/20 fixed top-0 left-0 backdrop-blur-sm flex justify-center items-center z-40">
                <div className="w-full h-full bg-transparent -z-10 absolute top-0 left-0"></div>
                <div
                    style={{ backgroundColor: "white" }}
                    className=" w-11/12 md:w-[60%] max-w-2xl rounded-2xl min-h-max max-h-[80%] overflow-auto"
                >
                    <div className="navigation p-5 bg-gradient-to-br from-emdms-prime20 to-emdms-prime40 w-full flex items-center justify-between sticky top-0 z-20">
                        <p className="w-full text-center font-semibold text-lg capitalize text-black">Add User</p>
                        <div
                            onClick={(e) => setShowModal(false)}
                            className="p-1 text-black hover:scale-100 transition-all ease-out rounded-full shadow-lg cursor-pointer shadow-red"
                        >
                            close
                        </div>
                    </div>

                    <div className="p-8 capitalize flex flex-col gap-8 z-10">
                        {/* username */}
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                                Name
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="username"
                                type="text"
                                placeholder="Username"
                                onChange={(e) => {
                                    userInfo.name = e.target.value;
                                    setUserInfo({ ...userInfo });
                                }}
                            />
                        </div>

                        {/* email */}
                        <div className="mb-4">
                            <label className="block text-gray-700 text-sm font-bold mb-2">
                                Email
                            </label>
                            <input
                                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                                id="email"
                                type="text"
                                placeholder="email"
                                onChange={(e) => {
                                    userInfo.email = e.target.value;
                                    setUserInfo({ ...userInfo });
                                }}
                            />
                        </div>

                        {/* button */}
                        <button
                            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded mb-4"
                            onClick={() => handleSubmit()}
                        >
                            Save User
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
};

// to build static website
export const getServerSideProps: GetServerSideProps = async (context) => {
    const res = await fetch("http://localhost:3001/users");
    const results: User[] = await res.json();

    console.log("result is .. ", results);

    return {
        props: {
            users: results,
        },
    };
};