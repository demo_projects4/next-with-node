import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';

interface Item {
  id: number;
  title: string;
  description: string;
}

export default function MessyPage(): JSX.Element {
  const router = useRouter();
  const [data, setData] = useState<Item[] | null>(null);
  const [loading, setLoading] = useState<boolean>(false);

 
  useEffect(() => {
    let isMounted = true;

    const getData = async () => {
      try {
        setLoading(true);
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        const data = await response.json();
        if (isMounted) {
          setData(data);
          setLoading(false);
        }
      } catch (error) {
        console.error('Error fetching data:', error);
        if (isMounted) {
          setLoading(false);
        }
      }
    };

    getData();

    return () => {
      isMounted = false;
    };
  }, []);

  return (
    <div style={{ fontFamily: 'Arial', color: 'black', backgroundColor: 'white', padding: '20px' }}>
      <h1 style={{ fontSize: '30px', color: 'blue' }}>Page Title</h1>
      {loading ? (
        <p>Loading...</p>
      ) : (
        data &&
        data.map((item: Item, i: number) => (
          <div key={i} style={{ margin: '10px 0', padding: '10px', border: '1px solid black' }}>
            <h2 style={{ fontSize: '25px' }}>{item.title}</h2>
            <p>{item.description}</p>
            <button
              onClick={() => router.push(`/detail/${item.id}`)}
              style={{ padding: '10px', backgroundColor: 'green', color: 'white', border: 'none', cursor: 'pointer' }}
            >
              View Detail
            </button>
          </div>
        ))
      )}
    </div>
  );
}