export interface User {
    id: number;
    name: String;
    email: String;
    avatar: String;
}

