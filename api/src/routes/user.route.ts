import express from "express"
import { fetchUsers, fetchUsersByID, addUser } from "../controllers/user.controller";


const router = express.Router();

// get request for users
router.get("/", fetchUsers )

// get request for single user
router.get("/:id", fetchUsersByID )

router.post("/", addUser )


export default router