import express, { Application, Request, Response } from 'express';
import entitiesRouter from './clean_me.js';  // import entities router
import  UserRouter  from './routes/user.route';
import cors from "cors"; // import cors to allo post request from the frontend
import bodyParser from "body-parser"; // to read json body

const app: Application = express();
app.use(cors());
app.use(bodyParser.json());

const PORT: number = 3001;

app.use('/', entitiesRouter);  // use entities router at path '/entities'


// user route
app.use("/users", UserRouter);

app.use('/', (req: Request, res: Response): void => {
    res.send('Hello world!');
});

app.listen(PORT, (): void => {
    console.log('SERVER IS UP ON PORT:', PORT);
});
