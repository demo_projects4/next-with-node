
import { Request, Response } from "express";

// define the  user model
export default class User {
    id: number = 0;
    name: String = ''
    email: String = ''
    avatar: String = ''
}

let users: User[] = [{
    "id": 1,
    "name": "jean",
    "email": "jean@example.com",
    "avatar": "https://placekitten.com/200/300"
}]


// define the controller 
export const fetchUsers = async (
    req: Request,
    res: Response,
    next: Function
) => {

    try {
        res.status(200).json(users);

    } catch (error) {
        next(error)
    }

}


// fetch users by id
export const fetchUsersByID = async (
    req: Request,
    res: Response,
    next: Function
) => {

    console.log("req ", req.params.id)
    try {

        // iterate over the users array and grab a user with specified id
        let user = users.find((user) => user.id == parseInt(req.params.id))

        console.log("user is ", user)
        res.status(200).json(user);

    } catch (error) {
        next(error)
    }

}

export const addUser = async (
    req: Request,
    res: Response,
    next: Function
) => {

    try {
        users.push({ id: users?.length + 1, name: req.body?.name, email: req.body?.email, avatar: req.body?.avatar });

        res.status(200).json(users);

    } catch (error) {
        next(error)
    }

}
